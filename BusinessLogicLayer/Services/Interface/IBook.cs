﻿using DataEntities.Models;

namespace BusinessLogicLayer.Services.Interface
{
    public interface IBook
    {
        Book GetBookDetails(int id);
        void AddBook(Book book);
        void UpdateBook(Book book);
        bool DeleteBook(int id);
        List<Book> GetBookList();
    }
}
