﻿using DataEntities.Models;

namespace BusinessLogicLayer.Services.Interface
{
    public interface IAuthor
    {
        // get update add delete
        Author GetAuthorDetails(int id);
        List<Book> GetAuthorBooks(int id);
        List<Author> GetAuthorList();
        void AddAuthor(Author author);
        void UpdateAuthor(Author author);
        void DeleteAuthor(int id);
        void DeleteAuthorBooks(int id);
    }
}
