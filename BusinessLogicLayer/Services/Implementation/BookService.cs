﻿using BusinessLogicLayer.Services.Interface;
using DataAccessLayer;
using DataEntities.Models;

namespace BusinessLogicLayer.Services.Implementation
{
    public class BookService : IBook
    {
        private readonly ApplicationDbContext _db;
        public BookService(ApplicationDbContext db)
        {
            _db = db;
        }

        public void AddBook(Book book)
        {
            _db.Books.Add(book);
            _db.SaveChanges();
        }

        public bool DeleteBook(int id)
        {
            var obj = _db.Books.Find(id);
            if (obj == null)
            {
                return false;
            }
            _db.Books.Remove(obj);
            _db.SaveChanges();
            return true;
        }

        public Book GetBookDetails(int id)
        {
            Book book = _db.Books.Find(id);
            return book;
        }

        public void UpdateBook(Book book)
        {
            _db.Books.Update(book);
            _db.SaveChanges();
        }

        public List<Book> GetBookList()
        {
            return _db.Books.ToList();
        }
    }
}
