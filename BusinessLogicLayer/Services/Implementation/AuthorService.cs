﻿using BusinessLogicLayer.Services.Interface;
using DataAccessLayer;
using DataEntities.Models;

namespace BusinessLogicLayer.Services.Implementation
{
    public class AuthorService : IAuthor
    {
        private readonly ApplicationDbContext _db;
        public AuthorService(ApplicationDbContext db)
        {
            _db = db;
        }

        public Author GetAuthorDetails(int id)
        {
            var author = _db.Authors.Find(id);
            return author;
        }

        public List<Author> GetAuthorList()
        {
            return _db.Authors.ToList();
        }
        public List<Book> GetAuthorBooks(int id)
        {
            var books = _db.Books.Where(book => book.AuthorId.Equals(id)).ToList();
            return books;
        }

        public void AddAuthor(Author author)
        {
            _db.Authors.Add(author);
            _db.SaveChanges();
        }

        public void UpdateAuthor(Author author)
        {
            _db.Authors.Update(author);
            _db.SaveChanges();
        }

        public void DeleteAuthor(int id)
        {
            var author = _db.Authors.Find(id);
            _db.Authors.Remove(author);
            _db.SaveChanges();
        }

        public void DeleteAuthorBooks(int id)
        {
            var booksOfThisAuthor = _db.Books.Where(book => book.AuthorId.Equals(id)).ToList();
            foreach (var book in booksOfThisAuthor)
            {
                _db.Books.Remove(book);
                _db.SaveChanges();
            }
        }
    }
}
