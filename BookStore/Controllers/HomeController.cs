﻿using BusinessLogicLayer.Services.Interface;
using DataEntities.Models;
using Microsoft.AspNetCore.Mvc;

namespace BookStore.Controllers
{
    public class HomeController : Controller
    {
        private readonly IBook _book;
        public HomeController(IBook book)
        {
            _book = book;
        }

        public IActionResult Index()
        {
            IEnumerable<Book> objBook = _book.GetBookList();
            return View(objBook);
        }
    }
}