﻿using BusinessLogicLayer.Services.Interface;
using DataEntities.Models;
using Microsoft.AspNetCore.Mvc;

namespace BookStore.Controllers
{
    public class AuthorController : Controller
    {
        private readonly IAuthor _author;

        public AuthorController(IAuthor author)
        {
            _author = author;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ShowAuthorDetails(int id)
        {
            ViewBag.booksOfAuthor = _author.GetAuthorDetails(id);
            ViewBag.aboutAuthor = _author.GetAuthorBooks(id);
            return View();
        }

        // GET
        public IActionResult UpdateAuthor(int id)
        {
            var author = _author.GetAuthorDetails(id);
            _author.UpdateAuthor(author);
            return View(author);
        }

        // POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateAuthor(Author author)
        {
            if (ModelState.IsValid)
            {
                _author.UpdateAuthor(author);
                return RedirectToAction("Index", "Home");
            }
            return View(author);
        }

        public IActionResult DeleteAuthor(int id)
        {
            _author.DeleteAuthor(id);
            _author.DeleteAuthorBooks(id);
            return RedirectToAction("Index", "Home");
        }

        // GET
        public IActionResult AddAuthor()
        {
            return View();
        }

        // POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddAuthor(Author author)
        {
            if (ModelState.IsValid)
            {
                _author.AddAuthor(author);
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
    }
}