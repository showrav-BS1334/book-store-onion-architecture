﻿using BusinessLogicLayer.Services.Interface;
using DataEntities.Models;
using Microsoft.AspNetCore.Mvc;

namespace BookStore.Controllers
{
    public class BookController : Controller
    {
        private readonly IBook _book;
        private readonly IAuthor _author;
        public BookController(IBook book, IAuthor author)
        {
            _book = book;
            _author = author;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ShowBookDetails(int id)
        {
            var book = _book.GetBookDetails(id);
            var author = _author.GetAuthorDetails(book.AuthorId);
            ViewBag.author = author;
            ViewBag.book = book;
            return View();
        }

        // GET
        public IActionResult UpdateBook(int id)
        {
            var book = _book.GetBookDetails(id);
            var authors = _author.GetAuthorList();
            ViewBag.authors = authors;
            ViewBag.book = book;
            return View();
        }

        // POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateBook(Book book)
        {
            if (ModelState.IsValid)
            {
                _book.UpdateBook(book);
                return RedirectToAction("Index", "Home");
            }
            return View(book);
        }

        public IActionResult DeleteBook(int id) // NotFound is used here only
        {
            if(_book.DeleteBook(id) == false)
            {
                return NotFound();
            }
            return RedirectToAction("Index", "Home");
        }

        // GET
        public IActionResult AddBook()
        {
            ViewBag.authors = _author.GetAuthorList();
            return View();
        }

        // POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddBook(Book book)
        {
            if (ModelState.IsValid)
            {
                _book.AddBook(book);
                return RedirectToAction("Index", "Home");
            }
            return View(book);
        }
    }
}